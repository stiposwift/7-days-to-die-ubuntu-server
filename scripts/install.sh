#!/bin/bash

function usage(){
    echo ""
    echo "./install.sh -U USERNAME [-P PASSWORD] [-n NAME] [-r SCHEDULE] [-s] [-a] [-b BETA]"
    echo ""
    echo "Installs 7 Days to Die dedicated server and it's dependencies on an Ubuntu instance."
    echo "Optionally installs as an auto loading service when OS boots"
    echo "Optionally restarts entire OS (and therefore 7dtd server) on a schedule"
    echo "" 
    echo "-U USERNAME   - The user to install the game / services as, if omitted, current user is assumed."
    echo "-G GROUP      - The group that the game / service user account belongs to, if omitted, current user is assumed."
    echo "-P PASSWORD   - The sudo password, will be asked if omitted."
    echo "-r SCHEDULE   - A restart schedule to apply, one of 'nightly' or 'weekly' or a CRON syntax. Weekly restarts are Monday at 3am, Nightly at 3am"
    echo "-n NAME       - An instance name for this server, defaults to 'new-server', allows multiple instances per machine."
    echo "-s            - Register this as a service"
    echo "-a            - Automatically update the dedicated server software, requires that a restart schedule is set."
    echo "-b BETA       - Configures the server to run under a beta version."
    echo "-c CONFIGFILE - Allows you to supply a pre-made serverconfig.xml instead of generating a clean one."
    echo ""
}

DO_SERVICE=0
AUTO_UPDATE=0
while getopts U:G:P:n:r:sab:c: flag; do
  case "${flag}" in
  U) USERNAME=${OPTARG};;
  G) GROUP=${OPTARG};;
  P) PASSWORD=${OPTARG};;
  n) NAME=${OPTARG};;
  r) SCHEDULE=${OPTARG};;
  s) DO_SERVICE=1;;
  a) AUTO_UPDATE=1;;
  b) BETA=${OPTARG};;
  c) CONFIGFILE=${OPTARG};;
  *) echo "Invalid flag found";;
  esac
done

[ -z "$CONFIGFILE" ] && CONFIGFILE="templates/serverconfig.xml"
[ -z "$USERNAME" ] && USERNAME=$(whoami)
[ -z "$GROUP" ] && GROUP=$(id -gn)
[ -z "$PASSWORD" ] && read -r -s -p "Password:" PASSWORD
[ -z "$NAME" ] && NAME="new-server"

DEP_PARAMS="-U \"$USERNAME\" -P \"$PASSWORD\" "
if [ "$AUTO_UPDATE" == "1" ]; then
  DEP_PARAMS="$DEP_PARAMS -a"
fi

if ! /bin/bash -c "./scripts/install-dependencies.sh $DEP_PARAMS"; then
  echo "Failed to install dependencies"
  exit 1
fi

INSTALL_PARAMS="-U \"$USERNAME\" -n \"$NAME\""
if [ -n "$BETA" ]; then
  INSTALL_PARAMS="$INSTALL_PARAMS -b \"$BETA\""
fi
if ! /bin/bash -c "./scripts/install-7dtd.sh $INSTALL_PARAMS"; then
  echo "Failed to install 7 Days to Die"
  exit 2
fi

SERVER_PARAMS="-U \"$USERNAME\" -G \"$GROUP\" -P \"$PASSWORD\" -c \"$CONFIGFILE\" -n \"$NAME\""
if [ "$DO_SERVICE" == "1" ]; then
  SERVER_PARAMS="$SERVER_PARAMS -s"
fi
if ! /bin/bash -c "./scripts/install-server.sh $SERVER_PARAMS"; then
  echo "Failed to register 7 Days to Die as a service"
  exit 3
fi

if [ -n "$SCHEDULE" ]; then
  REBOOT_PARAMS="-U \"$USERNAME\" -P \"$PASSWORD\" -r \"$SCHEDULE\" -n \"$NAME\""
  if [ "$AUTO_UPDATE" == "1" ]; then
    REBOOT_PARAMS="$REBOOT_PARAMS -a"
  fi
  if ! /bin/bash -c "./scripts/setup-reboot.sh $REBOOT_PARAMS"; then
    echo "Failed to setup auto restart";
    exit 4
  fi
fi
