#!/bin/bash

while getopts U:b:n: flag; do
    case "${flag}" in
    U) USERNAME=${OPTARG};;
    b) BETA=${OPTARG};;
    n) NAME=${OPTARG};;
    *) echo "Invalid flag found";;
    esac
done

[ -z "$USERNAME" ] && USERNAME=$(whoami)
[ -z "$NAME" ] && NAME="new-server"

HOMEDIR="/home/$USERNAME"
SERVERDIR="/$HOMEDIR/7dtd/$NAME"

# Copy scripts and config
mkdir -p "$SERVERDIR/game/logs"

if [ -n "$BETA" ]; then
  BETA_OPTS="-beta $BETA"
else
  BETA_OPTS=""
fi

# Update and Copy steamcmd template
sed -i -e "s/__USERNAME__/${USERNAME}/g" templates/7dtd.steamcmd
sed -i -e "s/__NAME__/${NAME}/g" templates/7dtd.steamcmd
sed -i -e "s/__BETA__/${BETA_OPTS}/g" templates/7dtd.steamcmd
cp templates/7dtd.steamcmd $SERVERDIR/7dtd.steamcmd

# install the game using steam command
echo "Running command $HOMEDIR/steamcmd/steamcmd.sh +runscript $SERVERDIR/7dtd.steamcmd"
echo "The command file looks like this:"
cat "$SERVERDIR/7dtd.steamcmd"
/bin/bash -c "$HOMEDIR/steamcmd/steamcmd.sh +runscript $SERVERDIR/7dtd.steamcmd"
