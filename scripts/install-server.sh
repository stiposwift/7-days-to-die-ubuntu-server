#!/bin/bash

while getopts U:G:P:n:sc: flag; do
    case "${flag}" in
    U) USERNAME=${OPTARG};;
    G) GROUP=${OPTARG};;
    P) PASSWORD=${OPTARG};;
    n) NAME=${OPTARG};;
    s) DO_SERVICE=1;;
    c) CONFIGFILE=${OPTARG};;
    *) echo "Invalid flag found";;
    esac
done

[ -z "$CONFIGFILE" ] && CONFIGFILE="templates/serverconfig.xml"
[ -z "$USERNAME" ] && USERNAME=$(whoami)
[ -z "$GROUP" ] && GROUP=$(id -gn)
[ -z "$PASSWORD" ] && read -s -r -p "Password:" PASSWORD
[ -z "$NAME" ] && NAME="new-server"

SERVERDIR="/home/$USERNAME/7dtd/$NAME"

sed -i -e "s/__USERNAME__/${USERNAME}/g" templates/7dtd.service
sed -i -e "s/__GROUP__/${GROUP}/g" templates/7dtd.service
sed -i -e "s/__NAME__/${NAME}/g" templates/7dtd.service
sed -i -e "s/__USERNAME__/${USERNAME}/g" templates/server.sh
sed -i -e "s/__NAME__/${NAME}/g" templates/server.sh
sed -i -e "s/__USERNAME__/${USERNAME}/g" templates/update.sh
sed -i -e "s/__NAME__/${NAME}/g" templates/update.sh

echo "Copying server files"
cp templates/server.sh $SERVERDIR/server.sh
cp "$CONFIGFILE" $SERVERDIR/serverconfig.xml
cp templates/update.sh $SERVERDIR/update.sh
if [ "$DO_SERVICE" == "1" ]; then
  echo "Creating service"
  # Delete the file if it exists
  echo "$PASSWORD" | sudo -S rm "/usr/lib/systemd/system/7dtd_$NAME.service"
  echo "$PASSWORD" | sudo -S cp templates/7dtd.service "/usr/lib/systemd/system/7dtd_$NAME.service"
  echo "$PASSWORD" | sudo -S systemctl start "7dtd_$NAME"
  echo "$PASSWORD" | sudo -S systemctl enable "7dtd_$NAME"
fi
