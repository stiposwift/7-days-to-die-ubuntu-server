#!/bin/bash

AUTO_UPDATE=0
while getopts U:P:a flag; do
    case "${flag}" in
    U) USERNAME=${OPTARG};;
    P) PASSWORD=${OPTARG};;
    a) AUTO_UPDATE=1;;
    *) echo "Invalid flag found";;
    esac
done
[ -z "$USERNAME" ] && USERNAME=$(whoami)
[ -z "$PASSWORD" ] && read -s -r -p "Password:" PASSWORD
DEPS="libc6-i386 libgcc1 lib32stdc++6 lib32gcc-s1"
if [ "$AUTO_UPDATE" == "1" ]; then
  DEPS="$DEPS cron"
fi
echo "Installing dependencies: $DEPS"
echo "$PASSWORD" | sudo -S dpkg --add-architecture i386
echo "$PASSWORD" | sudo -S apt-get update
echo "$PASSWORD" | sudo -S apt-get upgrade -y
echo "$PASSWORD" | sudo -S apt-get install -y $DEPS

HOMEDIR="/home/$USERNAME"
THISDIR=$(pwd)
# Install Steamcmd
mkdir -p "${HOMEDIR}/steamcmd"
cd "${HOMEDIR}/steamcmd"
wget http://media.steampowered.com/client/steamcmd_linux.tar.gz
tar -xf steamcmd_linux.tar.gz
rm steamcmd_linux.tar.gz
chmod +x ./steamcmd.sh
cd "${THISDIR}"
