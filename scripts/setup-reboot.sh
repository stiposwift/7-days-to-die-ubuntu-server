#!/bin/bash

function cron_me_daddy() {
  CRON_SCHEDULE=$1
  CRON_USER=$2
  CRON_JOB=$3
  CRON_SCRIPT=${@:4}

  echo "Setting up $CRON_JOB"
  echo "$CRON_SCHEDULE $CRON_USER $CRON_SCRIPT" > "$CRON_JOB"
  chmod 644 "$CRON_JOB"
  echo "$PASSWORD" | sudo -S chown root:root "$CRON_JOB"
  echo "$PASSWORD" | sudo -S mv "$CRON_JOB" "/etc/cron.d/$CRON_JOB"
}

AUTO_UPDATE=0
while getopts U:P:n:r:a flag; do
    case "${flag}" in
    U) USERNAME=${OPTARG};;
    P) PASSWORD=${OPTARG};;
    n) NAME=${OPTARG};;
    r) SCHEDULE=${OPTARG};;
    a) AUTO_UPDATE=1;;
    *) echo "Invalid flag found";;
    esac
done
[ -z "$USERNAME" ] && USERNAME=$(whoami)
[ -z "$PASSWORD" ] && read -s -r -p "Password:" PASSWORD
[ -z "$NAME" ] && NAME="new-server"


if [ -z "$SCHEDULE" ]; then
  SCHEDULE="nightly"
fi

REBOOT_SCHEDULE=$SCHEDULE
if [ "$SCHEDULE" == "nightly" ]; then
  SCHEDULE="0 3 * * *"
  REBOOT_SCHEDULE="0 4 * * *"
elif [ "$SCHEDULE" == "weekly" ]; then
  SCHEDULE="0 3 * * 1"
  REBOOT_SCHEDULE="0 4 * * 1"
fi
if [ "$AUTO_UPDATE" == "1" ]; then 
  cron_me_daddy "$SCHEDULE" "root" "7dtd_update_$NAME" "/home/$USERNAME/7dtd/$NAME/update.sh"
fi
cron_me_daddy "$REBOOT_SCHEDULE" "root" "zz_reboot" "shutdown -r now"
