# 7 Days to Die - Ubuntu Server
This automates the setup of all requirements of running a 7 Days to Die server on Ubuntu Server, minimal installation.

> Note that these instructions do NOT take full security into consideration, and if you are intending to run a public server, this setup should be considered INSECURE.

**In theory** this set of scripts should work on any linux distro with apt as it's package manager, bash as it's shell and systemd, however package names can differ between distributions so you might need to modify the `install-dependencies.sh` script with appropriate package names.

# Provisioning
1. Install Ubuntu Server either in a VM or on bare metal, see [official instructions](https://ubuntu.com/tutorials/install-ubuntu-server#1-overview) on how to do this.
    * Install the 'minimal' installation version. This is a cleaner way to get 
2. Install open-ssh on the server for easier access.
    * `$> sudo apt update && sudo apt install -y open-ssh`
    * Change the ssh port to something other than 22, this is the bare minimum you can do to protect your server.
        * `$> sudo nano /etc/ssh/sshd_config`
        * Uncomment the port line in this file and set to something else, preferrably in the thousands.
3. Optional, add a quick connect to Windows Terminal with Ubuntu WSL.
    * Settings > Add a new profile
        * Command line: `wsl.exe -d Ubuntu ssh -p <SSH_PORT_HERE> <SERVER_ADMIN_USER>@<SERVER_IP_HERE>`
        * You can generate an ssh key on this WSL and add it to the server if you'd like, or allow password auth in the ssh config.

# Setup
Clone this entire repo on the target server
```shell
git clone https://gitlab.com/stiposwift/7-days-to-die-ubuntu-server.git
```

## Configuration
Before running the install, you should setup your `serverconfig.xml` it is provided in this repository.
Edit that xml file using something like `nano` or `vim` changing the options you want and hit save.
> NOTE if you're generating a random world, your worldsize needs to be a multiple of 2! So `1024`, `2048`, `4096`, `8192`, etc... The larger the size the longer the bootup. Failure to properly calculate this will cause your world gen to FAIL.


## Install
Run `install.sh` providing the following options:

| Option        | Required | Description |
|---------------|----------|-------------|
| -U USERNAME   | No       | The username of the user to install the server under, defaults to current user (`whoami`) |
| -G GROUP      | No       | The group of the user to run the service as, defaults to the main group of the current user (`id -gn`) |
| -P PASSWORD   | No       | The password of the current user to perform sudo operations, will be prompted if not supplied |
| -n NAME       | No       | The instance name of the server, defaults to 'new-server' |
| -s            | No       | Registers your 7 Days to Die server as a service on the OS with `systemd`, causing it to automatically start when the OS is booted, recommended when setting a reboot schedule. The service file will be at `/usr/lib/systemd/system/7dtd.service` |
| -r SCHEDULE   | No       | A schedule to automatically reboot the entire OS on, either 'nightly' or 'weekly', this generates cronfiles and puts them in the `/etc/cron.d/` directory. |
| -a            | No       | A flag indicating to keep the server software automatically updated, the update is run on the same schedule as the reboot, and requires that the reboot option is set. Note that this takes into consideration the `BETA` flag, so if the beta is updated, this will update, however if a beta is moved to the stable channel, you will need to recreate your server to switch to that.  |
| -b BETA       | No       | A beta flag to install from steam, you can view available betas by going to the 7 Days to Die game page in your library and pressing Settings (the gear) > Properties > Betas. The value should be what is on the left of the dash (`-`) so in the dropdown if you see `latest_experimental - Unstable build` you would supply `-b latest_experimental` |
| -c CONFIGFILE | No       | A prefilled `serverconfig.xml` to use instead of the default one supplied by this repository. Make sure your `serverconfig.xml` is VALID XML and a [valid server config](https://7daystodie.fandom.com/wiki/Server:_serverconfig.xml).  Note that the configfile in this repository is from STABLE ALPHA 19 (latest when this was created) and may NOT represent the current serverconfig structure |


This will start your server immediately assuming nothing went wrong. 

# Troubleshooting

> How do I know things are working?

The first time your server starts, it needs to generate the world. Server log is stored in the `~/7dtd/<name>/game/logs` folder. You can watch this log by running the tail command
```shell
tail -f ~/7dtd/game/logs/output_`DATE`.txt
```

> My world is taking forever to generate!

World gen is quite slow in 7 Days to Die, even on top-of-the-line hardware. But having a bad server config can cause the generation to fail.
Particularly, check that you are using a proper world size for a random world gen. It *NEEDS* to be a multiple of `2`.

> I'm seeing a bunch of permission denied errors!

You probably fat fingered your password. Try again. If you still see it, submit an [issue](https://gitlab.com/stiposwift/7-days-to-die-ubuntu-server/-/issues/new) with the output you're seeing.

