#!/bin/sh

SERVERDIR="/home/__USERNAME__/7dtd/__NAME__/game"
cd "$SERVERDIR"

PARAMS=$@
CONFIGFILE="/home/__USERNAME__/7dtd/__NAME__/serverconfig.xml"
if [ -f "$CONFIGFILE" ]; then
  echo Using config file: $CONFIGFILE
else
  echo "Specified config file $CONFIGFILE does not exist."
  exit 1
fi

mkdir -p $SERVERDIR/logs

export LD_LIBRARY_PATH=.

$SERVERDIR/7DaysToDieServer.x86_64 -configfile=$CONFIGFILE \
                                   -logfile $SERVERDIR/logs/output_`date +%Y-%m-%d__%H-%M-%S`.txt \
                                   -quit \
                                   -batchmode \
                                   -nographics \
                                   -dedicated \
                                   $PARAMS
